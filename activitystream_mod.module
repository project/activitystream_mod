<?php

/**
 * @file
 * Code for the Activity Stream Moderation module.
 */

/**
 * Implements hook_form_alter().
 */
function activitystream_mod_form_alter(&$form, &$form_state, $form_id) {

  // Gets all activitystream bundles available.
  foreach (activitystream_services_load() as $service => $info) {

    $form_ids[] = 'activitystream_item_edit_' . $info['type'] . '_form';
  }

  // Adds the moderation buttons to the form actions.
  if (in_array($form_id, $form_ids)) {

    // Hides the activitystream_mod_moderated field in order to add two separate actions to the form.
    $form['activitystream_mod_moderated']['#access'] = FALSE;

    // Hides the Save button and calls its submit function right before the
    // custom_activitystream_moderation_submit_moderate();
    if (user_access('moderate activitystream items')) {
      $form['actions']['submit']['#access'] = FALSE;
    }

    $default_config = array(
      '#type' => 'submit',
      '#weight' => 10,
      '#submit' => array('activitystream_mod_submit_moderate'),
      '#access' => 'activitystream_mod_access',
    );

    $default_class = 'activitystream-mod-button';

    $form['actions']['approve'] = $default_config + array(
      '#value' => t('Approve'),
      '#attributes' => array(
        'class' => array($default_class, 'approve')
      ),
    );

    $form['actions']['disapprove'] = $default_config + array(
      '#value' => t('Disapprove'),
      '#attributes' => array(
        'class' => array($default_class, 'disapprove'),
      ),
    );
  }
}

/**
 * Form API submit callback for the Activity Stream form moderation buttons.
 */
function activitystream_mod_submit_moderate(&$form, &$form_state) {

  $activitystream = &$form_state['activitystream_item'];
  $destination = array();
  $options = array(
    'approve',
    'disapprove',
  );

  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  else {
    $destination = 'admin/content/activitystream';
  }

  $op = reset($form_state['triggering_element']['#parents']);

  if (in_array($op, $options)) {

    // Set the moderatin status to TRUE.
    form_set_value($form['activitystream_mod_moderated'], array($form['#entity']->language => array(0 => array('value' => 1))), $form_state);

    if ($op == 'disapprove') {
      $activitystream->status = 0;
    }
    else {
      $activitystream->status = 1;
    }
  }

  module_load_include('inc', 'activitystream', 'activitystream.admin');

  activitystream_item_form_submit($form, $form_state);
}

/**
 * Determines whether the given user has access to moderate an Activity Stream Item.
 */
function activitystream_mod_access() {

  if (user_access('moderate activitystream items', $account)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_permission().
 */
function activitystream_mod_permission() {

  return array(
    'moderate activitystream items' =>  array(
      'title' => t('Moderate Activity Stream Items'),
      'description' => t('Moderates all Activity Stream Items'),
    ),
  );
}

/**
 * Implements hook_entity_presave().
 */
function activitystream_mod_entity_presave($entity, $type) {

  if ($type == 'activitystream_item') {

    if ($entity->is_new) {
      // Always save the activitystream as unpubished.
      $entity->status = 0;
    }
  }
}

function activitystream_items_save_alter(&$activitystream) {

  // Set the moderation status to FALSE (Not moderated) if the item is new.
  if ($activitystream->is_new) {

    $activitystream->activitystream_mod_moderated[LANGUAGE_NONE][0]['value'] = 0;
  }
}
