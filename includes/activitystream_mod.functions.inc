<?php

/**
 * @file
 * Install, update, and uninstall functions for Activity Stream Moderation
 * module.
 */


/**
 * Helper function to add the activitystream_mod_moderated field instance
 * in activitystream_item bundles.
 */
function activitystream_mod_add_field() {

  $field = array(
    'field_name' => 'activitystream_mod_moderated',
    'type' => 'list_boolean',
    'locked' => TRUE,
    'settings' => array(
       //add the default values for the checkbox
      'allowed_values' => array(
        0 => 'Not Moderated',
        1 => 'Moderated',
      ),
    ),
  );

  // Creates the field if it's not exists.
  $field_info = field_info_field('activitystream_mod_moderated');
  if (empty($field_info)) {
    field_create_field($field);
  }
}

/**
 * Helper function to add the activitystream_mod_moderated field instance
 * in activitystream_item bundles.
 */
function activitystream_mod_add_instances() {

  module_load_include('module', 'activitystream', 'activitystream');

  // Creates the field if the first time.
  activitystream_mod_add_field();

  $services = activitystream_services_load();

  foreach ($services as $service => $info) {
    $instance = array(
      'field_name' => 'activitystream_mod_moderated',
      'entity_type' => 'activitystream_item',
      'bundle' => $service,
      'label' => t('Moderated'),
      'required' => FALSE,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'type' => 'option_onoff',
        'weight' => 10,
        'settings' => array(
          'display_label' => 1,
        ),
      ),
      'default_value' => array(
        0 => array(
          'value' => 0,
        ),
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
        ),
      ),
    );

    // Creates the field instance for the current available services.
    $instance_info = field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
    if (empty($instance_info)) {
      field_create_instance($instance);
    }
  }
}
